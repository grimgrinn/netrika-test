window.onload = function(){
	
	var menu = document.querySelectorAll('.menu-layout');
	var firstScreen = document.querySelectorAll('.first-screen');
	var firstScreenInn = document.querySelectorAll('.first-screen__inn');
	var entries = document.querySelectorAll('.entries')[0];
	var menuButtons = document.querySelectorAll('.menu-button');
	var addButton = document.querySelectorAll('.add-button')[0];
	var index =0;

	var formBlank = document.createElement('div');
		formBlank.className = "item";
		formBlank.innerHTML = '<label class="item-cap">Item #</label><textarea name="" id="" cols="30" rows="10" class="item-text"></textarea><button class="item-btn item-remove">Remove</button><button class="item-btn item-restore hidden">Restore</button>';

	var entryBlank = document.createElement('div');
		entryBlank.className = "entry";
		entryBlank.innerHTML = 'entry-test';	

	for(var i = 0; i < menuButtons.length; i++){
		menuButtons[i].addEventListener('click', function (){
			toggleClass(menuButtons, 'hidden');
			toggleClass(menu,'hidden');
			toggleClass(firstScreen,'menu-opened');
			console.log('click menu');

		});
	}


	addButton.addEventListener('click', function(){
		console.log('adding new from');
		var form = formBlank.cloneNode(true);
		var entry = entryBlank.cloneNode(true);
		var formButtons = form.querySelectorAll('.item-btn');
		var formTarea = form.querySelector('textarea');
		var formLabel = form.querySelectorAll('.item-cap')[0];
		formTarea.placeholder = "please enter the text for entry #" + (index+1);

		form.setAttribute('item-number',index);
		entry.setAttribute('entry-number',index);
		entry.innerHTML = '<span class="blank-text">me is entry #'+(index+1)+'</span>';

		
		this.parentNode.insertBefore(form, this.parentNode.firstChild);
		entries.insertBefore(entry, entries.firstChild);

		for(var i = 0; i < formButtons.length; i++){
				formButtons[i].addEventListener('click', function (){
					var itemNumber = this.parentNode.getAttribute('item-number');
					//console.log('GGGGGGG',this.parentNode.getAttribute('item-number'));			
					var myEntry = document.querySelector('.entry[entry-number="'+itemNumber+'"]');
				
					toggleClass(formButtons, 'hidden');
					toggleClass(myEntry, 'hidden');
					toggleDisabled(formTarea);

				});
			}

		formTarea.addEventListener('keyup', function(e){
			var itemNumber = this.parentNode.getAttribute('item-number');
			var myEntry = document.querySelector('.entry[entry-number="'+itemNumber+'"]');

			myEntry.innerHTML = this.value;
			if(myEntry.innerHTML.length == 0){
				myEntry.innerHTML = '<span class="blank-text">me is entry #'+(+itemNumber+1)+'</span>';
			}
			
		});
			
		formLabel.innerHTML += (index+1);
		index++;
				
	});
	
}


function toggleClass(elements, c){
	elements = elements.length ? elements : [elements];
	for(var i=0;i<elements.length;i++) {
				var res = hasClass(elements[i], c) ? removeClass(elements[i],c) : addClass(elements[i],c);
			}	
}

function hasClass(e, c){
	var classNames = e.className.split(' ');
	var res = false;
	classNames.forEach((e)=> {if(e == c) res = true;});
	return res;
}

function removeClass(elements, c){
	elements = elements.length ? elements : [elements];
	for(var i=0;i<elements.length;i++) {
			elements[i].classList.remove(c);
		}
}

function addClass(elements,c){
	elements = elements.length ? elements : [elements];
	for(var i=0;i<elements.length;i++) {
			elements[i].className += ' '+c;
		}
}

function toggleDisabled(elements){
	elements = elements.length ? elements : [elements];
	for(var i=0;i<elements.length;i++){
		elements[i].disabled = !elements[i].disabled;
	}
}
